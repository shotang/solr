package com.shotang.api;

import java.util.ArrayList;
import java.util.List;

import com.shotang.constants.RamUnit;

public class SolrRequestObject {
	
	private Integer is4G;
	private Float minRam;
	private Float maxRam;
	private RamUnit ramUnit;
	private Float minScreenSize;
	private Float maxScreenSize;
	private Integer minBatteryCapacity;
	private Integer maxBatteryCapacity;
	private Integer numOfSims;
	private Integer cityId;
	private Float minMrp;
	private Float maxMrp;
	private Float minPrice;
	private Float maxPrice;

	
	private Integer categoryId;
	private Integer parentCategoryId;
	private List<String> brands;
	
	private Integer sellerId = null;
	private Integer customerId = null;
	
	private Integer pageSize = Integer.MAX_VALUE;
	private Integer pageNumber = 0;
	
	private List<String> tags = new ArrayList();
	
	public Integer getIs4G() {
		return is4G;
	}
	public void setIs4G(Integer is4g) {
		is4G = is4g;
	}
	public Float getMinRam() {
		return minRam;
	}
	public void setMinRam(Float minRam) {
		this.minRam = minRam;
	}
	public Float getMaxRam() {
		return maxRam;
	}
	public void setMaxRam(Float maxRam) {
		this.maxRam = maxRam;
	}
	public RamUnit getRamUnit() {
		return ramUnit;
	}
	public void setRamUnit(RamUnit ramUnit) {
		this.ramUnit = ramUnit;
	}
	public Float getMinScreenSize() {
		return minScreenSize;
	}
	public void setMinScreenSize(Float minScreenSize) {
		this.minScreenSize = minScreenSize;
	}
	public Float getMaxScreenSize() {
		return maxScreenSize;
	}
	public void setMaxScreenSize(Float maxScreenSize) {
		this.maxScreenSize = maxScreenSize;
	}
	public Integer getMinBatteryCapacity() {
		return minBatteryCapacity;
	}
	public void setMinBatteryCapacity(Integer minBatteryCapacity) {
		this.minBatteryCapacity = minBatteryCapacity;
	}
	public Integer getMaxBatteryCapacity() {
		return maxBatteryCapacity;
	}
	public void setMaxBatteryCapacity(Integer maxBatteryCapacity) {
		this.maxBatteryCapacity = maxBatteryCapacity;
	}
	public Integer getNumOfSims() {
		return numOfSims;
	}
	public void setNumOfSims(Integer numOfSims) {
		this.numOfSims = numOfSims;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public List<String> getBrands() {
		return brands;
	}
	public void setBrands(List<String> brands) {
		this.brands = brands;
	}
	public Float getMinMrp() {
		return minMrp;
	}
	public void setMinMrp(Float minMrp) {
		this.minMrp = minMrp;
	}
	public Float getMaxMrp() {
		return maxMrp;
	}
	public void setMaxMrp(Float maxMrp) {
		this.maxMrp = maxMrp;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Float getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Float minPrice) {
		this.minPrice = minPrice;
	}
	public Float getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Float maxPrice) {
		this.maxPrice = maxPrice;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
}
