package com.shotang.api.autosuggest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileAutoSuggestDoc  extends AutoSuggestDoc{
	
    @SerializedName("url")
    @Expose
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MobileAutoSuggestDoc(AutoSuggestDoc doc) {
		super(doc);
		// TODO Auto-generated constructor stub
	}
	
	

}
