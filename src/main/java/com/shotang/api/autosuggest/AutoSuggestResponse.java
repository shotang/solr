package com.shotang.api.autosuggest;

import java.util.ArrayList;
import java.util.List;

public class AutoSuggestResponse {
	
	List<GroupedAutosuggestResponse> groups;
	
	public AutoSuggestResponse() {
		groups = new ArrayList<>();
	}

	public List<GroupedAutosuggestResponse> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupedAutosuggestResponse> groups) {
		this.groups = groups;
	}
	
	public void addGroupResponse(GroupedAutosuggestResponse group) {
		groups.add(group);
	}
	
	

}
