package com.shotang.api.autosuggest;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GroupedAutosuggestResponse {
	
	@JsonIgnore
	String groupName ;
	@JsonProperty("type")
	String groupValue;
	@JsonProperty("pageCount")
	Long pageCount;
	@JsonProperty("data")
	List<? extends AutoSuggestDoc> docs;
	
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupValue() {
		return groupValue;
	}
	public void setGroupValue(String groupValue) {
		this.groupValue = groupValue;
	}
	public List<? extends AutoSuggestDoc> getDocs() {
		return docs;
	}
	public void setDocs(List<? extends AutoSuggestDoc> docs) {
		this.docs = docs;
	}
	public Long getPageCount() {
		return pageCount;
	}
	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
	}
}
