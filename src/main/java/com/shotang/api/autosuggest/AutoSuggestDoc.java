package com.shotang.api.autosuggest;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AutoSuggestDoc {

	@SerializedName("entityId")
	@Expose
	private Long entityId;

	@SerializedName("entityName")
	@Expose
	private String entityName;

	@SerializedName("categoryId")
	@Expose
	private Long categoryId;

	@SerializedName("categotyName")
	@Expose
	private String categotyName;

	@SerializedName("parentCategoryId")
	@Expose
	private Long parentCategoryId;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("score")
	@Expose
	private Float score;

	@SerializedName("categoryLevels")
	@Expose
	private List<Long> categoryLevels;
	public Long getEntityId() {
		return entityId;
	}

	@Field("entity_id")
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getEntityName() {
		return entityName;
	}
	@Field("entity_name")
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	@Field("cat_id")
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategotyName() {
		return categotyName;
	}
	@Field("cat_name")
	public void setCategotyName(String categotyName) {
		this.categotyName = categotyName;
	}
	public Long getParentCategoryId() {
		return parentCategoryId;
	}
	@Field("parent_cat_id")
	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public String getName() {
		return name;
	}
	@Field("name")
	public void setName(String name) {
		this.name = name;
	}
	public Float getScore() {
		return score;
	}
	@Field("score")
	public void setScore(Float score) {
		this.score = score;
	}

	public AutoSuggestDoc() {}

	public AutoSuggestDoc(AutoSuggestDoc doc) {
		super();

		this.entityId = doc.entityId;
		this.entityName = doc.entityName;
		this.categoryId = doc.categoryId;
		this.categotyName = doc.categotyName;
		this.parentCategoryId = doc.parentCategoryId;
		this.name = doc.name;
		this.score = doc.score;
		this.categoryLevels = doc.categoryLevels;
	}

	public List<Long> getCatLevels() {
		return categoryLevels;
	}
	@Field("category_level")
	public void setCatLevels(List<Long> categoryLevels) {
		this.categoryLevels = categoryLevels;
	}
	

}
