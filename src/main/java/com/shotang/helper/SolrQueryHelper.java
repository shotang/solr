package com.shotang.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;

import com.shotang.api.SolrRequestObject;

public class SolrQueryHelper {

	public SolrQuery getSolrQueryFromRequest(SolrRequestObject requestObject){
		SolrQuery query = new SolrQuery();
		List<String> queryTokens = new ArrayList<>();
		String max,min,val;
		if(requestObject.getMaxRam() != null || requestObject.getMinRam() !=null){
			max = requestObject.getMaxRam() != null ? requestObject.getMaxRam().toString() : "*";
			min = requestObject.getMinRam() != null ? requestObject.getMinRam().toString() : "*";
			if(requestObject.getRamUnit()==null){
				throw new IllegalArgumentException("Ram unit cannot be null if max/min ram present");
			}
			queryTokens.add("ram_memory : ["+min+" TO "+max+"]");
			queryTokens.add("ram_memory_unit :"+requestObject.getRamUnit());
		}
		if(requestObject.getMaxScreenSize() != null || requestObject.getMinScreenSize() !=null){
			max = requestObject.getMaxScreenSize() != null ? requestObject.getMaxScreenSize().toString() : "*";
			min = requestObject.getMinScreenSize() != null ? requestObject.getMinScreenSize().toString() : "*";
			queryTokens.add("screen_size : ["+min+" TO "+max+"]");
		}
		if(requestObject.getMaxBatteryCapacity() != null || requestObject.getMinBatteryCapacity() !=null){
			max = requestObject.getMaxBatteryCapacity() != null ? requestObject.getMaxBatteryCapacity().toString() : "*";
			min = requestObject.getMinBatteryCapacity() != null ? requestObject.getMinBatteryCapacity().toString() : "*";
			queryTokens.add("battery_capacity : ["+min+" TO "+max+"]");
		}
		if(requestObject.getMaxMrp() != null || requestObject.getMinMrp() !=null){
			max = requestObject.getMaxMrp() != null ? requestObject.getMaxMrp().toString() : "*";
			min = requestObject.getMinMrp() != null ? requestObject.getMinMrp().toString() : "*";
			queryTokens.add("mrp_price : ["+min+" TO "+max+"]");
		}

		if(requestObject.getIs4G() != null){
			queryTokens.add("is_4g : "+requestObject.getIs4G());
		}
		if(requestObject.getNumOfSims() != null){
			val = requestObject.getNumOfSims().toString();
			queryTokens.add("no_of_sims : "+val);
		}
		if(requestObject.getCityId() != null){
			val = "city_"+requestObject.getCityId()+"_score";
			queryTokens.add(val+" : [0 TO *]");
			query.setSort(val, ORDER.desc);
		}
		
		if(requestObject.getCategoryId() != null){
			val = "category_id : "+requestObject.getCategoryId();
			queryTokens.add(val);
		}
		if(requestObject.getBrands() != null && !requestObject.getBrands().isEmpty()){
			val = "brand_name : ("+StringUtils.join(requestObject.getBrands(), " ")+")";
			queryTokens.add(val);
		}
		if(requestObject.getParentCategoryId() != null ){
			val = "parent_category_id : "+requestObject.getParentCategoryId();
			queryTokens.add(val);
		}
		if(requestObject.getSellerId() != null ){
			val = "seller_id : "+requestObject.getSellerId	();
			queryTokens.add(val);
		}
		if(requestObject.getCustomerId() != null ){
			val = "{!join from=seller_id to=seller_id fromIndex=seller_retailer}retailer_id:"+requestObject.getCustomerId();
			query.addFilterQuery(val);
			val = "-{!join from=seller_product_id to=seller_product_id fromIndex=seller_retailer_exceptions}retailer_id:"+requestObject.getCustomerId();
			query.addFilterQuery(val);
		}
		if(requestObject.getMaxPrice()!= null || requestObject.getMinPrice() !=null){
			max = requestObject.getMaxPrice() != null ? requestObject.getMaxPrice().toString() : "*";
			min = requestObject.getMinPrice() != null ? requestObject.getMinPrice().toString() : "*";
			String joinQuery = "-({!join from=product_id_copy to=product_id v=$priceQuery})";
			String priceQuery = "priceQuery=(price:[* TO "+min+"] AND quantity:[1 TO *])";
			query.addFilterQuery(joinQuery);
			val = "price :["+min+" TO "+max+"]";
			query.set("priceQuery",priceQuery);
			queryTokens.add(val);
		}
		query.setQuery(StringUtils.join(queryTokens, " AND "));
		
		String tagsQuery = "";
		if(!requestObject.getTags().isEmpty()){
			tagsQuery = "(tags : ("+ StringUtils.join(requestObject.getTags()," AND " ) + " ) OR tags : ( all ))";
		}
		
		if(!requestObject.getTags().isEmpty()){
			query.setQuery(query.getQuery()+" AND " +tagsQuery);
		}
		query.setRows(requestObject.getPageSize());
		query.setStart(requestObject.getPageNumber()*requestObject.getPageSize());
		query.set("wt", "json");
		query.set("fl", "product_id");
		return query;
	}
	
	public SolrQuery getSolrAutosuggestQuery(String prefix,String entityName,String categoryName, List<Integer> parentCategoryIds,Integer resultCount,Integer pageNumber, Integer cityId, String catLevels){
		SolrQuery query = new SolrQuery();
		List<String> queryTokens = new ArrayList<>();
		queryTokens.add(prefix);
		if(StringUtils.isNotBlank(entityName)){
			queryTokens.add("entity_name:\""+entityName+"\"");
		}
		if(StringUtils.isNotBlank(categoryName)){
			queryTokens.add("cat_name:\""+categoryName+"\"");
		}
		
		//catLevels are comma separated category Ids
		if(StringUtils.isNotBlank(catLevels)){
			String[] catIds = catLevels.split(",");
			queryTokens.add("category_level : ("+ StringUtils.join(catIds," OR " ) + ")");
		}
		if(parentCategoryIds!=null && !parentCategoryIds.isEmpty())
		{
			queryTokens.add("parent_cat_id : ("+ StringUtils.join(parentCategoryIds,"  " ) + " )");
		}
		query.setQuery(StringUtils.join(queryTokens, " AND "));
		query.setRequestHandler("/suggest");
		query.set("wt", "json");
		query.set("group",true);
		query.set("group.field", "entity_name");
		query.set("group.offset",resultCount*pageNumber);
		query.set("group.limit", resultCount);
		query.set("group.sort","city_"+cityId+"_score desc , score desc");
		return query;
	}
}
