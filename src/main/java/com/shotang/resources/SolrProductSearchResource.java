package com.shotang.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.shotang.api.ProductIds;
import com.shotang.api.SolrRequestObject;
import com.shotang.service.SolrProductRequestService;
import com.shotang.service.SolrSellerProductRequestService;


@Path("/search")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class SolrProductSearchResource {
	
	SolrProductRequestService solrRequestService ;
	SolrSellerProductRequestService solrSellerRequestService ;
	
	public SolrProductSearchResource (SolrProductRequestService solrRequestService,SolrSellerProductRequestService solrSellerRequestService){
		this.solrRequestService = solrRequestService;
		this.solrSellerRequestService =  solrSellerRequestService;
	}
	
	@POST
	@Path("getProductIds")
	public Response getProductsIds(SolrRequestObject request){
		try{
			List<ProductIds> idList = solrRequestService.getProductIdsForAttributes(request);
			return Response.ok(idList).build();
		}
		catch(Exception e){
			return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity(e.getMessage())
                    .build();
		}
		
	}
	@POST
	@Path("seller/getProductIds")
	public Response getSellerProductsIds(SolrRequestObject request){
		try{
			List<ProductIds> idList = solrSellerRequestService.getProductIdsForAttributes(request);
			return Response.ok(idList).build();
		}
		catch(Exception e){
			return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity(e.getMessage())
                    .build();
		}
		
	}

}
