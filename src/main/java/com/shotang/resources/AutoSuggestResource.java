package com.shotang.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import com.shotang.api.autosuggest.AutoSuggestResponse;
import com.shotang.service.AutoSuggestService;

@Path("/suggest")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class AutoSuggestResource {

	AutoSuggestService autoSuggestService ;
	
	public AutoSuggestResource (AutoSuggestService autoSuggestService){
		this.autoSuggestService = autoSuggestService;
	}
	
	@GET
	public Response suggest(@QueryParam("prefix") String prefix,@QueryParam("entity") String entityName,
			@QueryParam("category") String categoryName, @QueryParam("parentCategoryId") List<Integer> parentCategoryIds,
			@DefaultValue("10")@QueryParam("count") Integer resultCount,@DefaultValue("0") @QueryParam("page") Integer pageNumber,@DefaultValue("1") @QueryParam("cityId") Integer cityId, @QueryParam("catLevels") String catLevels){
		try{
			if(StringUtils.isNotEmpty(prefix)){
				AutoSuggestResponse autoSuggests = autoSuggestService.suggest(prefix,entityName,categoryName,parentCategoryIds,resultCount,pageNumber,cityId,catLevels);
				return Response.ok(autoSuggests).build();
			}else{
				return Response
	                    .status(Status.BAD_REQUEST)
	                    .entity("Prefix cannot be empty")
	                    .build();
			}
		}
		catch(Exception e){
			return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity(e.getMessage())
                    .build();
		}
		
	}

}
