package com.shotang;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import org.glassfish.jersey.filter.LoggingFilter;

import com.shotang.resources.AutoSuggestResource;
import com.shotang.resources.MobileAutoSuggestResource;
import com.shotang.resources.SolrProductSearchResource;
import com.shotang.service.AutoSuggestService;
import com.shotang.service.SolrProductRequestService;
import com.shotang.service.SolrSellerProductRequestService;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

/**
 * Hello world!
 *
 */
public class App extends Application<AppConfiguration>
{
	public static void main(final String[] args) throws Exception {
		new App().run(args);
	}
	
	@Override
	public String getName() {
		return "search";
	}
    
    @Override
	public void run(final AppConfiguration configuration, final Environment environment) throws FileNotFoundException, IOException {
    	
    	Properties prop = new Properties();
		String filename = "config.properties";
		prop.load(new FileInputStream(filename));		
		String solrAddress = configuration.getSolr_server().getServer();
		SolrProductRequestService productRequestService = new SolrProductRequestService(solrAddress+prop.getProperty("solr.product.core"));
		SolrSellerProductRequestService sellerproductRequestService = new SolrSellerProductRequestService(solrAddress+prop.getProperty("solr.seller.product.core"));
		AutoSuggestService autoSuggestService = new AutoSuggestService(solrAddress+prop.getProperty("solr.autosuggest.core"));
		AutoSuggestResource autoSuggestResource = new AutoSuggestResource(autoSuggestService);
		MobileAutoSuggestResource mobileAutoSuggestResource = new MobileAutoSuggestResource(autoSuggestService);
		SolrProductSearchResource productSearchResource = new SolrProductSearchResource(productRequestService,sellerproductRequestService);
		
		
		environment.jersey().register(productSearchResource);
		environment.jersey().register(autoSuggestResource);
		environment.jersey().register(mobileAutoSuggestResource);
		
		environment.jersey().register(new LoggingFilter(Logger.getLogger("InboundRequestResponse"), true));
	}
}
