package com.shotang;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class AppConfiguration extends Configuration {

	@Valid
	@NotNull
	private SolrConfiguration solr_server = new SolrConfiguration();
	
	

	@JsonProperty("solr_server")
	public SolrConfiguration getSolr_server() {
		return solr_server;
	}

	@JsonProperty("solr_server")
	public void setSolr_server(SolrConfiguration solr_server) {
		this.solr_server = solr_server;
	}




	public static class SolrConfiguration
	{
		@Valid
		@NotNull
		private String server = null;

		@JsonProperty
		public String getServer() {
			return server;
		}

		@JsonProperty
		public void setServer(String server) {
			this.server = server;
		}
		
		
	}

}
