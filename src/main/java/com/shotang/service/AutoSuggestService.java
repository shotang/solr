package com.shotang.service;

import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.shotang.api.autosuggest.AutoSuggestDoc;
import com.shotang.api.autosuggest.AutoSuggestResponse;
import com.shotang.api.autosuggest.GroupedAutosuggestResponse;
import com.shotang.helper.SolrQueryHelper;

public class AutoSuggestService {
	
	SolrQueryHelper solrQueryHelper;
	SolrClient client;
	DocumentObjectBinder documentObjectBinder = new DocumentObjectBinder();
	
	public AutoSuggestService(String solrAAddress){
		solrQueryHelper  = new SolrQueryHelper ();
		initiateSolrClient(solrAAddress);
	}

	private void initiateSolrClient(String solrAddress) {
		HttpSolrClient.Builder builder = new HttpSolrClient.Builder();
		builder.withBaseSolrUrl(solrAddress);
		client = builder.build();
	}
	
	public AutoSuggestResponse suggest(String prefix,String entityName,String categoryName, List<Integer> parentCategoryIds, Integer resultCount,Integer pageNumber, Integer cityId, String catLevels) throws Exception{
		SolrQuery query = solrQueryHelper.getSolrAutosuggestQuery(prefix,entityName,categoryName,parentCategoryIds,resultCount,pageNumber,cityId,catLevels);
		client.query(query);
		QueryResponse rsp = client.query(query);
		GroupResponse groupedResponse =  rsp.getGroupResponse();
		AutoSuggestResponse autoSuggestResponse = new AutoSuggestResponse();
		List<GroupCommand> groupCommands = groupedResponse.getValues();
		for(GroupCommand groupCommand : groupCommands){
			List<Group> groups = groupCommand.getValues();
			for(Group group: groups){
				GroupedAutosuggestResponse groupedAutosuggestResponse = new GroupedAutosuggestResponse();
				groupedAutosuggestResponse.setGroupName(groupCommand.getName());
				groupedAutosuggestResponse.setGroupValue(group.getGroupValue());
				Long pageCount = group.getResult().getNumFound()%resultCount == 0 ? group.getResult().getNumFound()/resultCount : group.getResult().getNumFound()/resultCount  +1;
				groupedAutosuggestResponse.setPageCount(pageCount);
				groupedAutosuggestResponse.setDocs(documentObjectBinder.getBeans(AutoSuggestDoc.class, group.getResult()));
				autoSuggestResponse.addGroupResponse(groupedAutosuggestResponse);
			}
		}
		return autoSuggestResponse;
	}

}
