package com.shotang.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.shotang.api.ProductIds;
import com.shotang.api.SolrRequestObject;
import com.shotang.helper.SolrQueryHelper;

public class SolrProductRequestService {
	
	SolrQueryHelper solrQueryHelper;
	SolrClient client;
	
	public SolrProductRequestService(String solrAAddress){
		solrQueryHelper  = new SolrQueryHelper ();
		initiateSolrClient(solrAAddress);
	}

	private void initiateSolrClient(String solrAddress) {
		HttpSolrClient.Builder builder = new HttpSolrClient.Builder();
		builder.withBaseSolrUrl(solrAddress);
		client = builder.build();
	}
	
	public List<ProductIds> getProductIdsForAttributes(SolrRequestObject request) throws Exception{
		LinkedHashSet<ProductIds> products = new LinkedHashSet<>();
		List<ProductIds> productsToReturn = new ArrayList<>();
		try {
			SolrQuery query = solrQueryHelper.getSolrQueryFromRequest(request);
			client.query(query);
			QueryResponse rsp = client.query(query);
			products = new LinkedHashSet<ProductIds>(rsp.getBeans(ProductIds.class));
			productsToReturn.addAll(products);
		} catch (SolrServerException | IOException e) {
			throw e;
		}
		return productsToReturn;
		
	}

}
