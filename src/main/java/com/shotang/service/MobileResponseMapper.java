package com.shotang.service;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.util.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shotang.api.autosuggest.AutoSuggestDoc;
import com.shotang.api.autosuggest.AutoSuggestResponse;
import com.shotang.api.autosuggest.GroupedAutosuggestResponse;
import com.shotang.api.autosuggest.MobileAutoSuggestDoc;

public class MobileResponseMapper {

	public static final String BASE_URL = "shotang://shotang.com/";

	public static interface EntityToUrlMapper
	{
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc);
	}
	
	public static enum Category
	{
		MOBILES(97),
		TABLETS(59),
		ACCESSORIES(121),
		REFURBISHED(167);
		
		int categoryId = 0;
		
		Category(int categoryId)
		{
			this.categoryId = categoryId;
		}
		
		public static Category getInstance(long categoryId)
		{
			Category returnValue = null;
			
			for(int i=0;i<Category.values().length; i++)
			{
				if(Category.values()[i].categoryId == (int)categoryId)
				{
					returnValue = Category.values()[i];
					break;
				}
			}
			return returnValue;
		}
	}


	public static enum Entity
	{
		BRAND(new BrandEntityToUrlMapper()),
		FEATURE(new SearchEntityToUrlMapper()),
		PRODUCT(new ProductEntityToUrlMapper()),
		CATEGORY(new CatalogEntityToUrlMapper()),
		DEFAULT(new DefaultEntityToUrlMapper());

		private EntityToUrlMapper mapper;

		Entity(EntityToUrlMapper mapper)
		{
			this.mapper = mapper;
		}
	}

	public static class DefaultEntityToUrlMapper implements EntityToUrlMapper
	{
		@Override
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc) {
			String newUri = BASE_URL;
			MobileAutoSuggestDoc mobileAutoSuggestDoc = new MobileAutoSuggestDoc(doc);
			mobileAutoSuggestDoc.setUrl(newUri);
			return mobileAutoSuggestDoc;
		}
	}

	public static class BrandEntityToUrlMapper implements EntityToUrlMapper
	{

		@Override
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc) {
			MobileAutoSuggestDoc returnValue = null;
			String newUri = null;
			try {
				URI oldUri = new URI(BASE_URL);
				URI resolved = null;
				if(Category.getInstance(doc.getParentCategoryId()) == Category.MOBILES || 
						Category.getInstance(doc.getParentCategoryId()) == Category.TABLETS ||
						Category.getInstance(doc.getParentCategoryId()) == Category.REFURBISHED)
				{
					resolved = oldUri.resolve("catalog/"+doc.getParentCategoryId()+"/all_brands/"+doc.getEntityId());
					newUri = resolved.toString();
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			if(!TextUtils.isEmpty(newUri))
			{
				returnValue = new MobileAutoSuggestDoc(doc);
				returnValue.setUrl(newUri);
			}
			return returnValue;
		}
	}

	public static class CatalogEntityToUrlMapper implements EntityToUrlMapper
	{
		@Override
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc) {
			MobileAutoSuggestDoc returnValue = null;
			String newUri = null;
			try {
				URI oldUri = new URI(BASE_URL);
				URI resolved = null;
				if(Category.getInstance(doc.getParentCategoryId()) == Category.ACCESSORIES
						&& doc.getCatLevels()!=null && doc.getCatLevels().size()>=3)
				{
					resolved = oldUri.resolve("catalog/"+doc.getParentCategoryId()+"/all_accessories/"+doc.getCatLevels().get(2));
					newUri = resolved.toString();
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			if(!TextUtils.isEmpty(newUri)) {
				returnValue = new MobileAutoSuggestDoc(doc);
				returnValue.setUrl(newUri);
			}
			return returnValue;
		}
	}

	public static class SearchApiRequest
	{
		@SerializedName("autoSuggestDoc")
		@Expose
		AutoSuggestDoc doc;
	}

	public static class SearchEntityToUrlMapper implements EntityToUrlMapper
	{
		@Override
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc) {
			String newUri = null;
			try {
				SearchApiRequest apiRequest = new SearchApiRequest();
				apiRequest.doc = doc;
				Gson gson = new Gson();
				String jsonString = gson.toJson(apiRequest);
				String encodedUrl = URLEncoder.encode(jsonString, "UTF-8");
				URI oldUri = new URI(BASE_URL);
				URI resolved = oldUri.resolve("results/"+encodedUrl);
				newUri = resolved.toString();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			MobileAutoSuggestDoc mobileAutoSuggestDoc = new MobileAutoSuggestDoc(doc);
			mobileAutoSuggestDoc.setUrl(newUri);
			return mobileAutoSuggestDoc;
		}
	}

	public static class ProductEntityToUrlMapper implements EntityToUrlMapper
	{
		@Override
		public MobileAutoSuggestDoc getAutoSuggestDoc(AutoSuggestDoc doc) {
			String newUri = null;
			try {
				URI oldUri = new URI(BASE_URL);
				URI resolved = oldUri.resolve("product/"+doc.getEntityId());
				newUri = resolved.toString();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			MobileAutoSuggestDoc mobileAutoSuggestDoc = new MobileAutoSuggestDoc(doc);
			mobileAutoSuggestDoc.setUrl(newUri);
			return mobileAutoSuggestDoc;
		}
	}



	public static AutoSuggestResponse modifyAutoSuggestResponse(AutoSuggestResponse autoSuggestResponse)
	{
		for (Iterator<GroupedAutosuggestResponse> iterator = autoSuggestResponse.getGroups().iterator(); iterator.hasNext();) {
			GroupedAutosuggestResponse groupedAutoSuggestResponse = iterator.next();

			if(groupedAutoSuggestResponse.getDocs()!=null && !groupedAutoSuggestResponse.getDocs().isEmpty())
			{
				List<MobileAutoSuggestDoc> mobileautoSuggestDocs = new ArrayList<>();
				for(AutoSuggestDoc autoSuggestDoc:groupedAutoSuggestResponse.getDocs())
				{
					Entity entity = getEntityFor(autoSuggestDoc);
					MobileAutoSuggestDoc mobileautoSuggestDoc = entity.mapper.getAutoSuggestDoc(autoSuggestDoc);
					if(mobileautoSuggestDoc!=null)
					{
						mobileautoSuggestDocs.add(mobileautoSuggestDoc);
					}
				}
				if(mobileautoSuggestDocs.isEmpty())
				{
					iterator.remove();
				}
				else
				{
					groupedAutoSuggestResponse.setDocs(mobileautoSuggestDocs);
				}
			}
		}

		return autoSuggestResponse;
	}

	public static Entity getEntityFor(AutoSuggestDoc autoSuggestDoc)
	{
		Entity entityType = Entity.DEFAULT;
		try
		{
			entityType = Entity.valueOf(autoSuggestDoc.getEntityName());
		}catch(IllegalArgumentException e)
		{
			if(entityType!=null)
			{
				if(autoSuggestDoc.getEntityName().equalsIgnoreCase("RAM") || 
						autoSuggestDoc.getEntityName().equalsIgnoreCase("BATTERY") || 
						autoSuggestDoc.getEntityName().equalsIgnoreCase("DISPLAY") ||
						autoSuggestDoc.getEntityName().equalsIgnoreCase("SIMS") || 
						autoSuggestDoc.getEntityName().equalsIgnoreCase("4g"))
				{
					entityType = Entity.FEATURE;
				}
			}
		}
		return entityType;
	}



}
